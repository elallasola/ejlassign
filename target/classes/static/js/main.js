function validate(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key)|| isEmpty(key)) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}

function isEmpty(str) {
    return (!str || 0 === str.length);
}

function fieldEmpty() {
    var x;
    x = document.getElementById("fbInput").value;
    if (x == "") {
        return false;
    };
}